//
//  ViewController.swift
//  AWSCognito
//
//  Created by Denis Khlopin on 21.07.18.
//  Copyright © 2018 Denis Khlopin. All rights reserved.
//

import UIKit
import AWSMobileClient
import AWSAuthCore
import AWSAuthUI

import AWSUserPoolsSignIn

struct UserInfo{
    var username:String
    var password:String
    
    var userId:String
    var accessKey:String
    var expiration:Date
    var secretKey:String
    var sessionKey:String
    var token:String
    
    init() {
        username = ""
        password = ""
        userId = ""
        accessKey = ""
        expiration = Date()
        secretKey = ""
        sessionKey = ""
        token = ""
    }
    
    var description:String {
        get{
            return "UserName = \(self.username), UserId = '\(self.userId)', Token = '\(self.token)'"
        }
    }
}

class ViewController: UIViewController, AWSCognitoUserPoolsSignInHandler,AWSCognitoIdentityPasswordAuthentication,AWSCognitoIdentityInteractiveAuthenticationDelegate {
    
    //UI
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtOutput: UITextView!
    
    @IBAction func actionSignUp(_ sender: Any) {
        if let userName = txtUsername.text, let password = txtPassword.text {            
            signUp(userName, password: password)
        }
    }
    
    func getDetails(_ authenticationInput: AWSCognitoIdentityPasswordAuthenticationInput, passwordAuthenticationCompletionSource: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>) {
        self.passwordAuthenticationCompletion = passwordAuthenticationCompletionSource
    }
    
    func startPasswordAuthentication() -> AWSCognitoIdentityPasswordAuthentication {
        return self        
    }
    
    func didCompleteStepWithError(_ error: Error?) {
        if let error = error as NSError? {
            if let message = error.userInfo["message"] as? String{
                DispatchQueue.main.async {
                    self.txtOutput.text = message
                }
            }
        }
    }
    
    @IBAction func actionAutoLogin(_ sender: Any) {
        if let userName = txtUsername.text, let password = txtPassword.text {
            signIn(userName, password: password)
        }
    }
    
    var userInfo:UserInfo = UserInfo()
    
    var passwordAuthenticationCompletion: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>?
    
    func handleUserPoolSignInFlowStart() {
        self.passwordAuthenticationCompletion?.set(result: AWSCognitoIdentityPasswordAuthenticationDetails(username: self.userInfo.username, password: self.userInfo.password))
    }
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //----
    func signIn(_ username:String,password:String){
        self.userInfo.username = username
        self.userInfo.password = password
        
        AWSCognitoUserPoolsSignInProvider.sharedInstance().setInteractiveAuthDelegate(self)
        
        AWSSignInManager.sharedInstance().login(signInProviderKey: AWSCognitoUserPoolsSignInProvider.sharedInstance().identityProviderName) {  [unowned self] (result, error) in
            
            if let error = error {
                print(error.localizedDescription)
            }else{
                if let credentials = result as? AWSCredentials {
                    
                    self.userInfo.accessKey = credentials.accessKey
                    self.userInfo.expiration = credentials.expiration!
                    self.userInfo.secretKey = credentials.secretKey
                    self.userInfo.sessionKey = credentials.sessionKey!
                    
                    self.userInfo.token = ""
                    if let token = AWSCognitoUserPoolsSignInProvider.sharedInstance().token().result{
                        self.userInfo.token = token as String
                    }
                    
                    
                    if let user = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool().currentUser(){
                        if let username = user.username {
                            self.userInfo.userId = username
                        }
                    }
                    DispatchQueue.main.async {
                        self.txtOutput.text = self.userInfo.description
                    }
                }
            }
        }
    }

    func signUp(_ username:String,password:String){
        var attributeName = "empty"
        if isValidEmail(value: username){
            attributeName = "email"
        }else{
            attributeName = "phone"
        }
        
        let pool = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool()
        let email = AWSCognitoIdentityUserAttributeType(name: attributeName, value: username)
        
        pool.signUp(username, password: password, userAttributes: [email], validationData: nil).continueWith { (task) -> Any? in
            if let error = task.error as NSError?{
                if let message = error.userInfo["message"] as? String{
                    DispatchQueue.main.async {
                        self.txtOutput.text = message
                    }
                }
            }else
                if let result = task.result  {
                    if (result.user.confirmedStatus != AWSCognitoIdentityUserStatus.confirmed) {
                        print(result)
                    } else {
                    }
            }
            return nil
        }
    }

    private func isValidEmail(value:String) -> Bool {
        let EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", EMAIL_REGEX)
        return emailTest.evaluate(with: value)
    }
    
    private func isValidePhone(value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluate(with: value)
    }
}

